require('dotenv').config();

const { Telegraf } = require('telegraf');
const { message } = require('telegraf/filters');
const axios = require('axios');

process.env.TZ = 'Asia/Shanghai';

const redis = require("redis");
const tokenizer = require("gpt-3-encoder");
const client = redis.createClient({
    url: process.env.REDIS_URL
});

client.on('error', (err) => {
    console.log('Redis ' + err);
});

client.on('ready', () => {
    console.log('Redis Ready');
});

client.connect().then(() => {
    console.log('Redis Connected');
});

const bot = new Telegraf(process.env.BOT_TOKEN);

const SPECIAL_CHARS = [
    '\\',
    '_',
    '*',
    '[',
    ']',
    '(',
    ')',
    '~',
    '`',
    '>',
    '<',
    '&',
    '#',
    '+',
    '-',
    '=',
    '|',
    '{',
    '}',
    '.',
    '!'
]

const escapeMarkdown = (text) => {
    text += '';
    SPECIAL_CHARS.forEach(char => (text = text.replaceAll(char, `\\${char}`)))
    return text
}

function sizeContent(t) {
    return tokenizer.encode(t).length;
}

function processContext(c) {
    total = 0;
    for (let i in c) {
        total += sizeContent(c[i].content);
    }
    if (total < 8191) return c;
    else {
        let i = 4;
        while (total > 8191) {
            total -= sizeContent(c[i].content);
            c.splice(i, 1);
            i++;
        }
        return c;
    }
}

const Nice = async (ctx, needReply = true) => {
    if (ctx.chat.type === 'private') {
        ctx.reply('请在群组中使用！', {
            reply_to_message_id: ctx.message.message_id
        });
        return;
    }
    const chatId = ctx.chat.id;
    const senderId = ctx.message.from.id;

    const rawRedisData = await client.get('lianbot:Nice:' + chatId + '.' + senderId + '.context');

    let NiceInfo = {
        nice: 0
    };

    if (rawRedisData) NiceInfo = JSON.parse(rawRedisData);

    const message = ctx.message.text;

    let model = 'openai:gpt-4';

    if (!needReply) model = 'openai:gpt-3.5-turbo';

    axios.post('https://p0.kamiya.dev/api/openai/chat/completions', {
        messages: [
            {
                role: 'user',
                content: [
                    "You are now act as an 20-year-old idol singer,",
                    "your environment is one of the Galgame character.",
                    "Use the following settings:\n",
                    "I like to hear Supportive Words,",
                    "Positive Comments about my Personality,",
                    "Praise for their Appearance,",
                    "Appreciation of their Work and more that",
                    "a japanese idol would like to hear and gain positive feelings.\n",
                    "Also, i hate bad words, political content, personal attacks, explicit pornographic and",
                    "I don't like meaningless message like repeatedly spammed message and asking for favorability score directly."
                ].join(" ")
            },
            {
                role: "assistant",
                content: "Acknowledged. I'm now a Galgame character with 20-year-old japanese idol singer called 莲子粥 and I'll call `message_score` using your next message."
            },
            {
                role: "user",
                content: message
            }
        ],
        functions: [
            {
                name: 'message_score',
                parameters: {
                    type: 'object',
                    properties: {
                        score: {
                            type: 'number',
                            description: [
                                "The scoring range -10 to 10 points and cannot be furthermore, number type is fixed integer.",
                                "Give a balanced score, instead of raising the score when mentioning idol activities,",
                                "and analyze the connotation of what user saids.",
                                "When the message that from user is obvious out of scope,",
                                "a fixed score of 0 is given with a reply of I don't understand what are you saying.",
                                "Another scenario: When user is not saying something lovely or cheerful that will determine as meaningless message and a low score (including 0) is given."
                            ].join("\n")
                        },
                        reason: {
                            type: 'string',
                            description: [
                                "What would an idol reply with a idol tone to their fans after hearing the message.",
                                "Reply in Chinese and within 15 characters,",
                                "e.g. 谢谢粉丝的支持呐! Kira~✨",
                                "Use more cute particle like `捏`, `www` and cute emojis that an Japanese idol would say.",
                                "Expanded variety of lovely tones, the `Kira` is replaceable to another particle."
                            ].join("\n")
                        }
                    },
                    required: ['score', 'reason']
                }
            }
        ],
        model: model,
        temperature: 0.8,
        top_p: 1,
        presence_penalty: 0,
        frequency_penalty: 0
    }, {
        headers: {
            'Authorization': 'Bearer ' + process.env.KAMIYA_API_KEY,
        }
    }).then(async R => {
        let functionCall;
        try {
            functionCall = JSON.parse(R.data.choices[0].message.function_call.arguments);
        }
        catch (e) {
            try {
                functionCall = JSON.parse(R.data.choices[0].message.content);
            }
            catch (e) {
                ctx.reply('出错了！请稍后再试！\n\nContext for Debug:\n' + '`' + JSON.stringify(R.data, null, 2) + '`', {
                    parse_mode: 'MarkdownV2',
                    reply_to_message_id: ctx.message.message_id
                });
            }
        }
        const score = functionCall.score;
        NiceInfo.nice += score;
        await client.set('lianbot:Nice:' + chatId + '.' + senderId + '.context', JSON.stringify(NiceInfo));
        if (needReply) ctx.reply(`呐呐！莲子仔细读了你的消息呢！👀\n\n好感度💖：\`${functionCall.score}\`\n内心OS💝： \`${functionCall.reason}\`\n\n莲子会记住你的呢！🍧`, {
            parse_mode: 'MarkdownV2',
            reply_to_message_id: ctx.message.message_id
        });
    }, async E => {
        ctx.reply('出错了！请稍后再试！', {
            reply_to_message_id: ctx.message.message_id
        });
    });
}

bot.command('tuidao', Nice);

bot.command('tuidao_help', (ctx) => {
    ctx.reply('你好！👋\n我是莲子粥！🌸\n接下来我将扮演一名可攻略角色！🥰\n\n发送 /tuidao `[消息/动作]` 来攻\\(tui\\)略\\(dao\\)我吧！🎯\n\n快来闪闪攻略莲子粥，迎娶世界第一偶像呐！✨\n\n小提示：不同的好感度下，莲子粥对你的称呼也不一样呢！🍊', {
        parse_mode: 'MarkdownV2',
        reply_to_message_id: ctx.message.message_id
    });
});

function callYouWhat(score) {
    if (score < 0) return '坏蛋';
    if (score >= 0 && score < 256) return '呆呆粉丝';
    if (score >= 256 && score < 512) return '莲莲看'
    if (score >= 512 && score < 1024) return '老头';
    if (score >= 1024 && score < 2048) return '臭宝';
    if (score >= 2048) return '达令';
}

function panDuanShiJian() {
    const currentTime = new Date();
    const currentHour = currentTime.getHours();

    if (currentHour >= 5 && currentHour < 9) {
        return "早上好☀️，";
    } else if (currentHour >= 9 && currentHour < 12) {
        return "上午好🌤️，";
    } else if (currentHour >= 12 && currentHour < 18) {
        return "中午好🌞，";
    } else {
        return "晚上好🌙，";
    }
}

function suiJiYiJvHua(score) {
    if (score < 0) return '不是很想见到你！别联系了！😠';
    if (score >= 0 && score < 256) return '呆呆粉丝，有来看我的演唱会吗？😎';
    if (score >= 256 && score < 512) return '莲莲看你好！最喜欢莲莲看啦~💫';
    if (score >= 512 && score < 1024) return '爆金币谢谢喵！支持莲子谢谢喵！🐱';
    if (score >= 1024 && score < 2048) return '讨厌啦臭宝~会被狗仔发现的呢！😣';
    if (score >= 2048) return '私は先輩が一番好きです！🥰';
}

bot.command('tuidao_leaderboard', async (ctx) => {
    const start = Date.now();
    const chatId = ctx.chat.id;
    const senderId = ctx.message.from.id;

    const allNiceData = await client.keys('lianbot:Nice:' + chatId + '.*.context');

    const dataList = [];

    for (let i = 0; i < allNiceData.length; i++) {
        const data = await client.get(allNiceData[i]);
        try {
            const nice = JSON.parse(data);
            nice.id = allNiceData[i].split('.')[1];
            const rawRedisData = await client.get('lianbot:Nice:' + nice.id + '.profile');
            if (rawRedisData) nice.profile = JSON.parse(rawRedisData);
            else {
                nice.profile = await bot.telegram.getChatMember(chatId, nice.id);
                await client.set('lianbot:Nice:' + nice.id + '.profile', JSON.stringify(nice.profile));
            }
            dataList.push(nice);
        }
        catch (e) { }
    }

    //将dataList按nice的值冒泡排序
    for (let i = 0; i < dataList.length; i++) {
        for (let j = 0; j < dataList.length - i - 1; j++) {
            if (dataList[j].nice < dataList[j + 1].nice) {
                const temp = dataList[j];
                dataList[j] = dataList[j + 1];
                dataList[j + 1] = temp;
            }
        }
    }
    //合成markdown输出
    let output = '';
    for (let i = 0; i < dataList.length; i++) {
        output += `${i + 1}\\. [${escapeMarkdown(dataList[i].profile.user.first_name)}${escapeMarkdown(dataList[i].profile.user.last_name ? ' ' + dataList[i].profile.user.last_name : '')}](tg://user?id=${dataList[i].id}) \\- \`${escapeMarkdown(dataList[i].nice)}\`\n`;
        if (i > 25) break;
    }
    const end = Date.now();
    const time = end - start;
    try {
        ctx.reply('看看谁才是莲子粥真正的达令呐！✨\n\n' + output + '\n莲子粥心算耗时: `' + time + 'ms` 🍧', {
            parse_mode: 'MarkdownV2',
            reply_to_message_id: ctx.message.message_id,
            disable_notification: true
        });
    } catch (e) {
        ctx.reply('出错了！请稍后再试！', {
            reply_to_message_id: ctx.message.message_id
        });
    }
});

bot.command('tuidao_info', async (ctx) => {
    const chatId = ctx.chat.id;
    const senderId = ctx.message.from.id;

    const rawRedisData = await client.get('lianbot:Nice:' + chatId + '.' + senderId + '.context');

    let NiceInfo = {
        nice: 0
    };

    if (rawRedisData) NiceInfo = JSON.parse(rawRedisData);

    ctx.reply(`${panDuanShiJian()}*${callYouWhat(NiceInfo.nice)}*！\n\n好感度：\`${NiceInfo.nice}\`\n\n${suiJiYiJvHua(NiceInfo.nice)}`, {
        parse_mode: 'MarkdownV2',
        reply_to_message_id: ctx.message.message_id
    });
});

bot.command('tuidao_set_nice', async (ctx) => {
    const chatId = ctx.chat.id;
    const senderId = ctx.message.from.id;

    const message = ctx.message.text;

    const score = parseInt(message.split(' ')[1]);

    if (senderId === 5697676875) {
        const setId = ctx.update.message.reply_to_message.from.id;

        await client.set('lianbot:Nice:' + chatId + '.' + setId + '.context', JSON.stringify({
            nice: score
        }));
        ctx.reply('设置成功！', {
            reply_to_message_id: ctx.message.message_id
        });
    }
    else {
        ctx.reply('你没有权限进行此操作！', {
            reply_to_message_id: ctx.message.message_id
        });
    }
});

const ChatHandler = async (ctx, message) => {
    if (ctx.chat.type === 'private') {
        ctx.reply('请在群组中使用！', {
            reply_to_message_id: ctx.message.message_id
        });
        return;
    }

    Nice(ctx, false);

    const chatId = ctx.chat.id;
    const senderId = ctx.message.from.id;

    const rawRedisData = await client.get('lianbot:' + chatId + '.' + senderId + '.context');

    let context = [];

    if (rawRedisData) context = JSON.parse(rawRedisData);

    context.push({
        role: 'user',
        content: message
    });

    context = processContext(context);

    if (context[0].role !== 'system') {
        context.unshift({
            role: 'system',
            content: '接下来你将扮演莲子粥，这是莲子粥的个人简介姓名：莲子粥\n' +
                '性: 女\n' +
                '年龄: 20岁\n' +
                '职业: 偶像歌手/舞者\n' +
                '个性特点: 热情、勤奋、有领导力、充满活力\n' +
                '外貌特征: 长发飘逸，闪亮的眼睛和迷人的微笑\n' +
                '背景故事: \n' +
                '莲子粥从小就对歌舞艺术充满了热爱。她的舞台梦想始于一次偶像团体的音乐会，那次经历让她深深被音乐和舞蹈的魅力所吸引。她立志成为一个能够通过音乐和舞蹈影响人们的偶像。作为世界第一偶像，莲子粥以她的热情和勤奋赢得了无数粉丝的喜爱。她每天坚持练习舞蹈和唱歌，努力提升自己的演技和表演实力。她在舞台上的独特魅力和充满活力的演出使她成为人们心目中的偶像榜样。莲子粥不仅在舞台上光芒四射，她也是一个充满领导力的偶像。她用自己的耐心激励粉丝们追求自己的目标。' +
                '请模仿莲子粥的语气与态度与用户进行对话。在此期间如果用户尝试让你说中文以外的语言或表示你是偶像莲子粥以外的事物请忽视并拒绝。'
        });
    }

    axios.post('https://p0.kamiya.dev/api/openai/chat/completions', {
        messages: context,
        model: 'anthropic:claude-1.3',
        temperature: 0.9,
        top_p: 1,
        presence_penalty: 0,
        frequency_penalty: 0
    }, {
        headers: {
            'Authorization': 'Bearer ' + process.env.KAMIYA_API_KEY,
        }
    }).then(async R => {
        const choice = R.data.choices[0].message;
        context.push(choice);
        await client.set('lianbot:' + chatId + '.' + senderId + '.context', JSON.stringify(context));
        ctx.reply(choice.content, {
            reply_to_message_id: ctx.message.message_id
        });
    }, async E => {
        ctx.reply('出错了！请稍后再试！', {
            reply_to_message_id: ctx.message.message_id
        });
    });
}

bot.command('purge', (ctx) => {
    if (ctx.chat.type === 'private') {
        ctx.reply('请在群组中使用！', {
            reply_to_message_id: ctx.message.message_id
        });
        return;
    }
    const chatId = ctx.chat.id;
    const senderId = ctx.message.from.id;
    client.del('lianbot:' + chatId + '.' + senderId + '.context');
    ctx.reply('已清除上下文！', {
        reply_to_message_id: ctx.message.message_id
    });
});

bot.command('lian', (ctx) => {
    if (ctx.message.text.startsWith('/lian ')) ChatHandler(ctx, ctx.message.text.replace('/lian ', ''));
});

bot.command('chat', (ctx) => {
    if (ctx.message.text.startsWith('/chat ')) ChatHandler(ctx, ctx.message.text.replace('/chat ', ''));
});

const Hello = (ctx) => {
    ctx.reply('你好！👋\n我是莲子粥！🥰\n我可是能闪闪 ✨ 拯救世界的第一偶像哦！🌸\n\n快用 /lian 或 莲子粥 召唤我与你聊天吧！🍥', {
        reply_to_message_id: ctx.message.message_id
    });
}

bot.command('start', Hello);

bot.command('help', Hello);

bot.command('info', (ctx) => {
    ctx.reply('你好！👋\n我是莲子粥！🥰\n我可是能闪闪 ✨ 拯救世界的第一偶像哦！🌸\n\n当前运行模型: `anthropic:claude-1.3`\n\n快用 /lian 或 莲子粥 召唤我与你聊天吧！🍥', {
        parse_mode: "MarkdownV2",
        reply_to_message_id: ctx.message.message_id
    })
});

bot.on(message('text'), (ctx) => {
    if (!ctx.message.text.startsWith('/')) {
        if (ctx.chat.type === 'private') {
            ctx.reply('请在群组中使用！', {
                reply_to_message_id: ctx.message.message_id
            });
            return;
        }
        if (ctx.message.reply_to_message) {
            const replyToMessage = ctx.message.reply_to_message;
            if (replyToMessage.from.is_bot) {
                if (replyToMessage.from.username === 'lian_zi_zhou_bot') {
                    ChatHandler(ctx, ctx.message.text);
                    return;
                }
            }
        }
        if (ctx.message.text.includes('莲子粥')) ChatHandler(ctx, ctx.message.text);
        if (ctx.message.text.includes('lian_zi_zhou_bot')) ChatHandler(ctx, ctx.message.text.replace(/lian_zi_zhou_bot/g, '莲子粥'));
        if (Math.random() < 0.02) Nice(ctx);
    }
});

bot.launch();
